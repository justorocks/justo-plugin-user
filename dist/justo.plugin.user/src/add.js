"use strict";

var _dogmalang = require("dogmalang");

function add(params, ssh) {
  _dogmalang.dogma.paramExpectedToHave("params", params, { name: { type: _dogmalang.text, mandatory: true }, passwd: { type: _dogmalang.text, mandatory: false }, group: { type: _dogmalang.text, mandatory: false }, home: { type: [_dogmalang.text, _dogmalang.bool], mandatory: false }, shell: { type: _dogmalang.text, mandatory: false }, sys: { type: _dogmalang.bool, mandatory: false }, uid: { type: _dogmalang.num, mandatory: false }, gecos: { type: _dogmalang.text, mandatory: false } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    const $aux1511179343548 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux1511179343548 == "alpine") {
      alpine(params, ssh);
    } else if ($aux1511179343548 == "ubuntu") {
      debian(params, ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }
}module.exports = exports = add;
function alpine(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, _dogmalang.map);_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    let cmd = "sudo adduser -D";if (params.home) {
      cmd += " -h " + params.home;
    }if (params.home == false) {
      cmd += " -H";
    }if (params.shell) {
      cmd += " -s " + params.shell;
    }if (params.sys) {
      cmd += " -S";
    }if (params.uid) {
      cmd += " -u " + params.uid;
    }if (params.gecos) {
      cmd += (0, _dogmalang.fmt)(" -g '%s'", params.gecos);
    }cmd += " " + params.name;if (params.group) {
      cmd += " " + params.group;
    }if (params.passwd) {
      cmd = (0, _dogmalang.fmt)("%s && (echo '%s:%s' | sudo chpasswd)", cmd, params.name, params.passwd);
    }(ssh || _dogmalang.exec)(cmd);
  }
}
function debian(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, _dogmalang.map);_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    let cmd = "sudo adduser --quiet --disabled-login";if (params.home) {
      cmd += " --home " + params.home;
    }if (params.home == false) {
      cmd += " --no-create-home";
    }if (params.shell) {
      cmd += " --shell " + params.shell;
    }if (params.sys) {
      cmd += " --system";
    }if (params.uid) {
      cmd += " --uid " + params.uid;
    }if (params.group) {
      cmd += " --ingroup " + params.group;
    }cmd += (0, _dogmalang.fmt)(" --gecos '%s'", params.gecos || "");cmd += " " + params.name;if (params.passwd) {
      cmd = (0, _dogmalang.fmt)("(%s && (echo '%s:%s' | sudo chpasswd)) 2> /dev/null", cmd, params.name, params.passwd);
    }(ssh || _dogmalang.exec)(cmd);
  }
}
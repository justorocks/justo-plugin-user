"use strict";

var _dogmalang = require("dogmalang");

function exists(params, ssh) {
  let res = false;_dogmalang.dogma.paramExpectedToHave("params", params, { name: { type: _dogmalang.text, mandatory: true } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    const $aux15111793434173 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111793434173 == "alpine") {
      res = id(params, ssh);
    } else if ($aux15111793434173 == "ubuntu") {
      res = id(params, ssh);
    }
  }return res;
}module.exports = exports = exists;
function id(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    return _dogmalang.dogma.getItem(_dogmalang.dogma.peval(() => {
      return (ssh || _dogmalang.exec)("id " + params.name);
    }), 0);
  }
}
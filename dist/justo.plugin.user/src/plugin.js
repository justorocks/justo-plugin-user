"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.del = exports.exists = exports.add = undefined;

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const add = (0, _justo.simple)({ ["id"]: "add", ["desc"]: "Add a user.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Add '%s' user", params.name);
    }
  } }, _dogmalang.dogma.use(require("./add")));exports.add = add;

const exists = (0, _justo.simple)({ ["id"]: "exists", ["desc"]: "Check whether a user exists.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Check whether '%s' exists", params.name);
    }
  } }, _dogmalang.dogma.use(require("./exists")));exports.exists = exists;

const del = (0, _justo.simple)({ ["id"]: "del", ["desc"]: "Remove a user.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Remove '%s' user if exists", params.name);
    }
  } }, _dogmalang.dogma.use(require("./del")));exports.del = del;
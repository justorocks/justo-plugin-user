"use strict";

var _dogmalang = require("dogmalang");

function del(params, ssh) {
  _dogmalang.dogma.paramExpectedToHave("params", params, { name: { type: _dogmalang.text, mandatory: true }, home: { type: _dogmalang.bool, mandatory: false } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    const $aux15111793433171 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111793433171 == "alpine") {
      alpine(params, ssh);
    } else if ($aux15111793433171 == "ubuntu") {
      debian(params, ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }
}module.exports = exports = del;
function alpine(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    let cmd = "sudo deluser";if (params.home) {
      cmd += " --remove-home";
    }cmd += " " + params.name;{
      const [ok, res] = _dogmalang.dogma.peval(() => {
        return (ssh || _dogmalang.exec)(cmd);
      });if (!ok) {
        if (!_dogmalang.dogma.includes(res, "unknown")) {
          _dogmalang.dogma.raise(res);
        }
      }
    }
  }
}
function debian(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    let cmd = "sudo deluser -q";if (params.home) {
      cmd += " --remove-home";
    }cmd += " " + params.name;{
      const [ok, res] = _dogmalang.dogma.peval(() => {
        return (ssh || _dogmalang.exec)(cmd);
      });if (!ok) {
        if (!_dogmalang.dogma.includes(res, "does not exist")) {
          _dogmalang.dogma.raise(res);
        }
      }
    }
  }
}
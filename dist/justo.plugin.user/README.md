# justo.plugin.user

**Justo** plugin for working with OS users.
It is compatible with **Alpine** and **Ubuntu**.
It is local and SSH-compatible.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Proudly made with ♥ in Valencia, Spain, EU.*

## Use

```
const user = require("justo.plugin.user");
```

## user.exists task

This task checks whether a user exists:

```
user.exists({name}) : bool
```

- `name` (string, required). User name to check.

Example:

```
if (!user.exists({name: "myuser"})) {
  user.add({name: "myuser", passwd: "mypasswd"});
}
```

## user.add task

This task add a user:

```
user.add({name, passwd, group, home, shell, sys, uid, gecos})
```

- `name` (string, required). User name.
- `passwd` (string). Password.
- `group` (string). Group where to add the user.
- `home` (string or bool). If string, home dir; if false, not creating home dir.
- `shell` (string). Shell to set.
- `sys` (bool). System user? Default: `false`.
- `uid` (number). UID.
- `gecos` (string). GECOS field.

Example:

```
user.add({
  name: "myuser",
  passwd: "mypasswd",
  shell: "/bin/bash",
  sys: true
});
```

## user.del task

This task removes a user if it exists:

```
user.del({name, home})
```

- `name` (string, required). User name.
- `home` (bool). Remove home dir?

Example:

```
user.del({
  name: "myusr",
  home: true
});
```
